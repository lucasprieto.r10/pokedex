//
//  pokemonMananger.swift
//  pokedex
//
//  Created by Bootcamp 3 on 2022-11-02.
//

import Foundation

protocol pokemonManangerDelegado {
    func mostrarListaPokemon(lista:[pokemon])
    
}
struct pokemonMananger {
    var delegado : pokemonManangerDelegado?
    
    func verPokemon(){
        let urlString = "https://pokedex-bb36f.firebaseio.com/pokemon.json"
        
        if let url = URL(string: urlString){
            let seccion = URLSession(configuration: .default)
            let tarea = seccion.dataTask(with: url){ datos, respuesta, error in
                if error != nil {
                    print("error al obtener datos ",error?.localizedDescription)
                }
                if let datosseguros = datos?.parseData(quitarString: "null,"){
                    if let listaPokemon = self.parsearJSON(datosPokemon: datosseguros){
                        print("listapokemon ",listaPokemon)
                        delegado?.mostrarListaPokemon(lista: listaPokemon)
                    }
                }
                    
            }
            tarea.resume()
        }
            
        
    }
    func parsearJSON(datosPokemon: Data) -> [pokemon]?{
        let decodificador = JSONDecoder()
        do{
            let datosDecodificados = try decodificador.decode([pokemon].self, from: datosPokemon)
            return datosDecodificados
        }catch {
            print("error al decodificar",error.localizedDescription)
            return nil
        }
        
        
    }
}

extension Data {
    func parseData(quitarString palabra: String)-> Data? {
      let dataAsString = String(data: self, encoding: .utf8)
      let parseDateString = dataAsString?.replacingOccurrences(of: palabra, with: "")
      guard let data = parseDateString?.data(using: .utf8) else {return nil}
      return data
    }
  }
