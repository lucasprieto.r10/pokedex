//
//  ViewController.swift
//  pokedex
//
//  Created by Bootcamp 3 on 2022-11-02.
//

import UIKit

class ViewController: UIViewController {
    

    @IBOutlet weak var tablapokemon: UITableView!
    
    @IBOutlet weak var searchbarpokemon: UISearchBar!
    
    var Pokemonmananger = pokemonMananger()
    
    var pokemons: [pokemon] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tablapokemon.register(UINib(nibName: "celdaPokemonTableViewCell", bundle: nil), forCellReuseIdentifier: "celda")
        
        
        Pokemonmananger.delegado = self
        tablapokemon.delegate = self
        tablapokemon.dataSource = self
        Pokemonmananger.verPokemon()

    }


}
extension ViewController : pokemonManangerDelegado {
    func mostrarListaPokemon(lista: [pokemon]) {
        pokemons = lista
        
        
        DispatchQueue.main.async {
            self.tablapokemon.reloadData()
        }

        
    }
    
    
}

extension ViewController : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = tablapokemon.dequeueReusableCell(withIdentifier: "celda", for: indexPath) as! celdaPokemonTableViewCell
        celda.nombrepokemon.text = pokemons[indexPath.row].name
        celda.ataquepokemon.text = "ataque :\(pokemons[indexPath.row].attack)"
        celda.defensapokemon.text = "defensa :\(pokemons[indexPath.row].defense)"
        
        if let urlString = pokemons[indexPath.row].imageUrl as? String{
            if let imagenURL = URL(string: urlString){
                DispatchQueue.global().async {
                    guard let imagenData = try? Data(contentsOf: imagenURL) else{
                        return}
                        let image = UIImage(data: imagenData)
                        DispatchQueue.main.sync {
                            celda.imagenpokemon.image = image
                        }
                    }
                }
            }
                
            
        
        
        return celda
        
    }
    
    
}

