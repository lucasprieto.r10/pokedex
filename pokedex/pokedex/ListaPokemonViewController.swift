//
//  ViewController.swift
//  pokedex
//
//  Created by Bootcamp 3 on 2022-11-02.
//

import UIKit

class ViewController: UIViewController {
    

    @IBOutlet weak var tablapokemon: UITableView!
    
    @IBOutlet weak var searchbarpokemon: UISearchBar!
    
    
    
  // MARK: Vriables
    var pokemonManager = PokemonManager()
    var pokemons : [Pokemon] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // registrar la nueva celda
        tablapokemon.register(UINib (nibName: "CeldaPokemonTableViewCell", bundle: nil), forCellReuseIdentifier: "celda")
        
        
        pokemonManager.delegado = self
        
        tablapokemon.delegate = self
        tablapokemon.dataSource = self
        // Ejecutar el metodo para buscar la lista de pokmeon en la api
        pokemonManager.verPokemon()
    }


}

// MARK: Delegado Pokemon
extension ViewController : pokemonManagerDelegado {
    func mostrarListaPokemon(lista: [Pokemon]) {
        pokemons = lista
        DispatchQueue.main.async {
            self.tablapokemon.reloadData()
        }
    }
    
    
}

extension ViewController : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = tablapokemon.dequeueReusableCell(withIdentifier: "celda", for: indexPath) as! CeldaPokemonTableViewCell
        celda.nombrePokemon.text = pokemons[indexPath.row].name
        celda.ataquePokemon.text = "Ataque: \(pokemons[indexPath.row].attack)"
        celda.defensaPokemon.text = "Defensa: \(pokemons[indexPath.row].defense)"
        
        //celda.imagenPokemon.image = UIImage (named: "bulbassor")
        // celda.imagen desde url
        if let urlString = pokemons[indexPath.row].imageUrl as? String {
            if let imagenURL = URL(string: urlString) {
                DispatchQueue.global().async {
                    guard let imagenData = try? Data (contentsOf: imagenURL) else
                    { return }
                    let image = UIImage(data: imagenData)
                    DispatchQueue.main.async {
                        celda.imagenPokemon.image = image
                    }
                }
            }
        }
        
        return celda
        
    }
    
    
}

